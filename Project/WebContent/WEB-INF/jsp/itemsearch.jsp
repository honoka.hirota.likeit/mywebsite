<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>商品検索</title>
<link rel="stylesheet"
href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">
    <link rel="stylesheet" href="./css/index.css">
</head>
<body style="background-color: snow">
 <nav class="navbar navbar-light" style="background-color: black;">
        <div class="nav">
		<li class="dropdown" style="list-style: none">
            <a href="Index" class="navbar-link index-link" style="color: white; font-size: 40px"><p class="sample09; font-italic">COCO CLOSET</p></a></li>　
           </div>
		<div class="nav justify-content-end">
            <li class="navbar-text" style="color: darkgray">${userInfo.name}さん
			</li>　　
			<li class="dropdown"><a href="Logout"
				class="navbar-link logout-link" style="color: #ff3300;">ログアウト</a></li>
		</div>
	</nav>
        <br>
    <br>
    <div align="center">
     <form class="form-signin" action="ItemSearch" method="GET">
         <div class="container">
             <div class="row center">
				<div class="input-group flex-nowrap">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="addon-wrapping">🔍</span>
                 	  <input type="text" name="search_word" class="form-control" placeholder="検索" aria-label="検索" aria-describedby="addon-wrapping">
                    </div>
				</div>
			</div>
			<br>
            <br>
		</div>
	 </form>
			<h2>検索結果</h2>
			<p>
				検索結果${itemCount}件
			</p>
			<div class="container" >
		<div class="center">
        <br>
		<div class="section">
         <!--   商品情報   -->
			<div class="row center">
				<c:forEach var="item" items="${itemList}" varStatus="status">
				<div class="col s12 m3">
					<div class="card" style="width: 15rem; background-color: ghostwhite">
						<div class="card-image">
							<a href="Item?id=${item.id}"><img src="img/${item.fileName}" width="128" height="128"></a>
						</div>
						<div class="card-content">
							<span class="card-title">${item.name}</span>
							<p>${item.formatPrice}円
							</p>
						</div>
					</div>
				</div>
				<c:if test="${(status.index + 1) % 4 == 0}">
			</div>
			<br>
			<div class="row center">
				</c:if>
				</c:forEach>
				</div>
			</div>
		</div>
            <br>
			<nav aria-label="Page navigation example">
  <ul class="pagination justify-content-center">
  <!-- １ページ戻るボタン  -->
  	<c:choose>
		<c:when test="${pageNum == 1}">
			<li class="disabled"><a class="page-link" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
		</c:when>
		<c:otherwise>
			<li class="page-item"><a class="page-link" href="ItemSearch?search_word=${searchWord}&page_num=${pageNum - 1}" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
		</c:otherwise>
	</c:choose>
	<!-- ページインデックス -->
     <li class="page-link"><c:forEach begin="${(pageNum - 5) > 0 ? pageNum - 5 : 1}" end="${(pageNum + 5) > pageMax ? pageMax : pageNum + 5}" step="1" varStatus="status">
					<c:if test="${pageNum == status.index }"> <a class="active" href="ItemSearch?search_word=${searchWord}&page_num=${status.index}">${status.index} </a></c:if>
				</c:forEach></li>
	<!-- 1ページ送るボタン -->
    <c:choose>
		<c:when test="${pageNum == pageMax || pageMax == 0}">
			<li class="disabled"><a class="page-link" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
		</c:when>
		<c:otherwise>
			<li class="page-item"><a class="page-link" href="ItemSearch?search_word=${searchWord}&page_num=${pageNum + 1}" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
		</c:otherwise>
	</c:choose>
  </ul>
</nav>
<br>
    </div>
</body>
</html>