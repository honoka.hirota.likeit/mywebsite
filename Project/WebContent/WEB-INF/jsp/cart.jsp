<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>カート</title>
<link rel="stylesheet"
href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">
<link rel="stylesheet" href="./css/index.css">
</head>
<body style="background-color: snow">
 <nav class="navbar navbar-light" style="background-color: black;">
        <ul class="nav">
		<li class="dropdown" style="list-style: none">
            <a href="Index" class="navbar-link index-link" style="color: white; font-size: 40px"><p class="sample09; font-italic">COCO CLOSET</p></a></li>　
           </ul>
		<ul class="nav justify-content-end">
            <li class="navbar-text" style="color: darkgray">${userInfo.name}さん
			</li>　　
			<li class="dropdown"><a href="Logout"
				class="navbar-link logout-link" style="color: #ff3300;">ログアウト</a></li>
		</ul>
	</nav>
        <br>
	   <br>
    <br>
	<div class="container">
		<div align="center">
			${cartActionMessage}
			<br>
			<br>
			<h2 class=" col s12 light">カート</h2>
		</div>
        <br>
        <div align="center">
		<div class="section">
		<form action="ItemDelete" method="POST">
				<div class="row">
					<div class="col s12">
						<div class="col s6 center-align">
						<button class="btn btn-outline-secondary" type="submit" name="action">　
								削除　</button>　
							<a href="Index"><button type="button" class="btn btn-outline-warning">買い物を続ける
							</button></a>
							　<a href="Buy"><button type="button" class="btn btn-outline-info">　レジに進む　</button></a>
						</div>
					</div>
				</div>
                <br>
                <br>
				<div class="row center">
					<c:forEach var="item" items="${cart}" varStatus="status">
					<div class="col s12 m3">
						<div class="card" style="width: 15rem; background-color: ghostwhite">
							<div class="card-image">
								<a href="Item?item_id=${item.id}"><img src="img/${item.fileName}" width="140" height="140"> </a>
							</div>
							<div class="card-content">
								<span class="card-title">${item.name}</span>
								<p>${item.formatPrice}円
								</p>
								<p>
									<input type="checkbox" id="${status.index}" name="delete_item_id_list" value="${item.id}" /> <label for="${status.index}">削除</label>
								</p>
							</div>
						</div>
					</div>
					<c:if test="${(status.index+1) % 4 == 0 }">
					</div>
					<br>
				<div class="row center">
					</c:if>
					</c:forEach>
				</div>
			</form>
		</div>
	</div>
	</div>
    <br>
</body>
</html>