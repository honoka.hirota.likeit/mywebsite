<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>商品 削除</title>
<link rel="stylesheet"href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">
    <link rel="stylesheet" href="test.css">
</head>
<body style="background-color: snow">
 <nav class="navbar navbar-light" style="background-color: black;">
        <div class="nav">
		<li class="dropdown" style="list-style: none">
            <a href="Index" class="navbar-link index-link" style="color: white; font-size: 40px"><p class="sample09; font-italic">COCO CLOSET</p></a></li>　
           </div>
		<div class="nav justify-content-end">
            <li class="navbar-text" style="color: darkgray">${userInfo.name}さん
			</li>　　
			<li class="dropdown"><a href="Logout"
				class="navbar-link logout-link" style="color: #ff3300;">ログアウト</a></li>
		</div>
	</nav>
    <br>
    <br>
    <br>
    <div class="container">
    <div align="center">
     <div class="delete">
         <h2>商品削除確認</h2>
            <br>
         <br>
 	<form class="form-signin" action="MItemDelete" method="post">
        <div>
         <p>商品ID : ${item.id}<br>
          <input type="hidden" name="id" value="${item.id}">
             を本当に削除してよろしいでしょうか。</p>
            </div>
            <br>
                <br>
        <div class="center-row">
           <input type="submit" class="btn btn-outline-primary" value="　　OK　　">　　　
         <a href="MItemList" class="btn btn-outline-success">キャンセル</a>
        </div>
    </form>
         </div>
         </div>
    </div>
</body>
</html>