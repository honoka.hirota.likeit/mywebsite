<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>商品マスタ一覧</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">
</head>
<body style="background-color: snow">
 <nav class="navbar navbar-light" style="background-color: black;">
        <ul class="nav">
		<li class="dropdown" style="list-style: none">
            <a href="Index" class="navbar-link index-link" style="color: white; font-size: 40px"><p class="sample09; font-italic">COCO CLOSET</p></a></li>　
    </ul>
		<ul class="nav justify-content-end">
            <li class="navbar-text" style="color: darkgray">${userInfo.name}さん
			</li>　　
			<li class="dropdown"><a href="Logout"
				class="navbar-link logout-link" style="color: #ff3300;">ログアウト</a></li>
		</ul>
	</nav>
        <br>
    <br>
    <br>
     <div align="center">
         <h2>商品一覧</h2>
         <div align="right"><a class="btn btn-link" href = "MItemRegist">新規登録　</a></div>
		<form class="form-signin" action="MItemList" method="post">
			<div>
				<label>商品名　<input type="text" name="search_word"></label>
			</div>
			<br>
			<button type="submit" class="btn btn-info">検索</button>
		</form>
		<br>
            </div>
            <hr>
          <div class="table-responsive">
             <table class="table table-striped">
               <thead>
                <tr>
                  <th>商品ID</th>
                  <th>商品名</th>
                  <th>商品価格</th>
                  <th></th>
                </tr>
 			   </thead>
               <tbody>
                 <c:forEach var="item" items="${itemList}" >
                   <tr>
                     <td>${item.id}</td>
                     <td>${item.name}</td>
                     <td>${item.formatPrice}円</td>
                     <td>
                       <a class="btn btn-outline-primary" href="MItemDetail?id=${item.id}">　詳細　</a>　
                       <a class="btn btn-outline-success" href="MItemUpdate?id=${item.id}">　更新　</a>　
                        <a class="btn btn-outline-warning" href="MItemDelete?id=${item.id}">　削除　</a>
                     </td>
                   </tr>
                 </c:forEach>
               </tbody>
             </table>
           </div>
</body>
</html>