<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>スタート画面</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="./css/index.css">
</head>
<body style="background-color: snow">
    <nav class="navbar navbar-light" style="background-color: black;">
        <ul class="nav">
		<li class="dropdown" style="list-style: none">
            <a href="Index" class="navbar-link index-link" style="color: white; font-size: 40px"><p class="sample09; font-italic">COCO CLOSET</p></a></li>　
         <li class="nav-item active" style="list-style: none">
        <a class="nav-link" href="Cart" style="font-size: 18px">カート <span class="sr-only">(current)</span></a>
      </li>
        <li class="nav-item active" style="list-style: none">
        <a class="nav-link" href="UserData" style="font-size: 18px">会員情報 <span class="sr-only">(current)</span></a>
      </li>
      <c:if test="${userInfo.loginId=='admin' || userInfo.loginId==user.loginId}">
      <li class="nav-item active" style="list-style: none">
            <a class="nav-link" href="MUserDetailList" style="font-size: 18px">会員情報一覧<span class="sr-only">(current)</span></a>
      </li>
       <li class="nav-item active" style="list-style: none">
        <a class="nav-link" href="MItemList" style="font-size: 18px">商品一覧 <span class="sr-only">(current)</span></a>
      </li>
      </c:if>
       </ul>
		<ul class="nav justify-content-end">
            <li class="navbar-text" style="color: darkgray">${userInfo.name}さん
			</li>　　
			<li class="dropdown"><a href="Logout"
				class="navbar-link logout-link" style="color: #ff3300;">ログアウト</a></li>
		</ul>
	</nav>
     <div align="center">
    <div class="section no-pad-bot" id="index-banner">
		<div class="container">
			<br> <br>
			<h1 style="color: black" class="sample09; font-italic">COCO CLOSET</h1>
			<div class="row center">
				<h6 class="header col s12 light">洋服通販サイト</h6>
			</div>
            <br>
            <c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert" style="color: #ff3300;">${errMsg}</div>
		</c:if>
		 <form class="form-signin" action="ItemSearch" method="GET">
			<div align="center">
         <div class="container">
             <div class="row center">
				<div class="input-group flex-nowrap">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="addon-wrapping">🔍</span>
                 	  <input type="text" name="search_word" class="form-control" placeholder="検索" aria-label="検索" aria-describedby="addon-wrapping">
                    </div>
				</div>
			</div>
			<br>
            <br>
		</div>
	</div>
	 </form>
	<div class="container">
		<div class="row center">
			<h5 class=" col s12 light">おすすめ商品</h5>
		</div>
        <br>
		<div class="section">
			<!--   おすすめ商品   -->
			<form class="form-signin" action="Item" method="GET">
			<div class="row">
				<c:forEach var="item" items="${itemList}">
				<div class="col s12 m3">
					<div class="card" style="background-color: ghostwhite">
						<div class="card-image">
							<a href="Item?id=${item.id}"><img src="img/${item.fileName}" width="128" height="128"></a>
						</div>
						<div class="card-content">
							<span class="card-title">${item.name}</span>
							<p>${item.formatPrice}円
							</p>
						</div>
					</div>
				</div>
				</c:forEach>
			</div>
			</form>
		</div>
	</div>
    </div>
    </div>
    </div>
    </body>
</html>