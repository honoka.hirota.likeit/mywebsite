<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>商品情報参照</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">
</head>
<body style="background-color: snow">
	<nav class="navbar navbar-light" style="background-color: black;">
        <ul class="nav">
		<li class="dropdown" style="list-style: none">
            <a href="Index" class="navbar-link index-link" style="color: white; font-size: 40px"><p class="sample09; font-italic">COCO CLOSET</p></a></li>　
    </ul>
		<ul class="nav justify-content-end">
            <li class="navbar-text" style="color: darkgray">${userInfo.name}さん
			</li>　　
			<li class="dropdown"><a href="Logout"
				class="navbar-link logout-link" style="color: #ff3300;">ログアウト</a></li>
		</ul>
	</nav>
	<br>
	<br>
	<br>
	<div align="center">
		<h2>商品情報 詳細</h2>
		<br>
		<div>
			<label>商品ID : ${item.id}</label>
		</div>
		<div>
			<label>商品名 : ${item.name}</label>
		</div>
		<div>
			<label>商品詳細 : ${item.detail}</label>
		</div>
		<div>
			<label>価格 : ${item.price}円</label>
		</div>
		<div>
			<img src="img/${item.fileName}" width="128" height="128" >
		</div>
		<br>
	</div>
	<div align="left">
		<a class="btn btn-link" href="MItemList"> <u>戻る</u>
		</a>
	</div>
</body>
</html>
