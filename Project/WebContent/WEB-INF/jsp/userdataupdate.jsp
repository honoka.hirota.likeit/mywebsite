<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ情報更新</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">
</head>
<body style="background-color: snow">
 <nav class="navbar navbar-light" style="background-color: black;">
        <ul class="nav">
		<li class="dropdown" style="list-style: none">
            <a href="Index" class="navbar-link index-link" style="color: white; font-size: 40px"><p class="sample09; font-italic">COCO CLOSET</p></a></li>　
           </ul>
		<ul class="nav justify-content-end">
            <li class="navbar-text" style="color: darkgray">${userInfo.name}さん
			</li>　　
			<li class="dropdown"><a href="Logout"
				class="navbar-link logout-link" style="color: #ff3300;">ログアウト</a></li>
		</ul>
	</nav>
    <br>
	   <br>
	       <br>
	<div align="center">
		<h2>ユーザ情報更新</h2>
		<br>
		<c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert" style="color: #ff3300;">${errMsg}</div>
		</c:if>
		<form class="form-signin" action="UserDataUpdate" method="post">
			<div>
				<label>ログインID : 　${udb.loginId}</label>
				<input type="hidden" name="loginId" value="${udb.loginId}">
			</div>
			<div>
				<label>パスワード</label> <input type="password" name="password">
			</div>
			<div>
				<label>パスワード(確認)</label> <input type="password" name="confirmation">
			</div>
			<div>
				<label>名前</label>
				<input type="text" name="name" value="${udb.name}">
			</div>
			<div>
				<label>生年月日</label>
				<input type="date" name="birthDate" value="${udb.birthDate}">
			</div>
			<div>
				<label>住所</label>
				<input type="text" name="address" value="${udb.address}">
			</div>
			<br>
			<p>
               <button type="submit" class="btn btn-info">　更新　</button>
			 </p>
		</form>
		<br>
	</div>
	<div align="left">
		<a class="btn btn-link" href="UserData"> <u>戻る</u>
		</a>
	</div>
</body>
</html>