<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>購入</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
    <body style="background-color: snow">
        <nav class="navbar navbar-light" style="background-color: black;">
        <ul class="nav">
		<li class="dropdown" style="list-style: none">
            <a href="Index" class="navbar-link index-link" style="color: white; font-size: 40px"><p class="sample09; font-italic">COCO CLOSET</p></a></li>　
           </ul>
		<ul class="nav justify-content-end">
            <li class="navbar-text" style="color: darkgray">${userInfo.name}さん
			</li>　　
			<li class="dropdown"><a href="Logout"
				class="navbar-link logout-link" style="color: #ff3300;">ログアウト</a></li>
		</ul>
	</nav>
	<br>
	<br>
        <br>
        <div align="center">
	<div class="container">
	<form action="BuyConfirm" method="post">
		<div class="row center">
		${cartActionMessage}
			<br>
			<br>
			<h2 class=" col s12 light">購入</h2>
		</div>
        <br>
        <br>
        <table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">商品名</th>
      <th scope="col">単価</th>
      <th scope="col">小計</th>
    </tr>
  </thead>
  <tbody>
      <c:forEach var="cartInItem" items="${cart}">
    <tr>
      <td>${cartInItem.name}</td>
      <td>${cartInItem.formatPrice}円</td>
      <td>${cartInItem.formatPrice}円</td>
    </tr>
        </c:forEach>
      <tr>
        <td class="center"></td>
        <td class="center"></td>
        <td class="right">
            <label>配送先 :　${udb.address}</label>
          </td>
      </tr>
    <tr>
        <td class="center"></td>
        <td class="center"></td>
        <td class="right">
            <div class="input-field">
				<label>配送方法</label>
                <select name="delivery_method_id">
				    <c:forEach var="dmdb" items="${dmdbList}" >
				        <option value="${dmdb.id}">${dmdb.name}</option>
				    </c:forEach>
				</select>
            </div>
        </td>
      </tr>
  </tbody>
</table>
        <br>
    <br>
        <div class="row">
            <div class="col s12">
				    <button type="submit" class="btn btn-info">　購入確認　</button>
                <br>
            </div>
        </div>
        </form>
    </div>
</div>
    <div align="left">
		<a class="btn btn-link" href = "Cart">
			<u>戻る</u>
		</a>
	</div>
</body>
</html>