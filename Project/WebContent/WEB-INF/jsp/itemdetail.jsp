<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>商品詳細</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">
</head>
<body style="background-color: snow">
 <nav class="navbar navbar-light" style="background-color: black;">
        <ul class="nav">
		<li class="dropdown" style="list-style: none">
            <a href="Index" class="navbar-link index-link" style="color: white; font-size: 40px"><p class="sample09; font-italic">COCO CLOSET</p></a></li>　
           </ul>
		<ul class="nav justify-content-end">
            <li class="navbar-text" style="color: darkgray">${userInfo.name}さん
			</li>　　
			<li class="dropdown"><a href="Logout"
				class="navbar-link logout-link" style="color: #ff3300;">ログアウト</a></li>
		</ul>
	</nav>
     <div align="center">
        <div class="section no-pad-bot" id="index-banner">
			<br>
                <br>
            <br>
				</div>
					<h2 class=" col s12 light">商品詳細</h2>
			<br>
         <br>
			<div class="row">
				<div class="col s6">
					<div class="card">
						<div class="card-image">
							<img src="img/${item.fileName}"width="350" height="350">
						</div>
					</div>
				</div>
				<div class="col s6">
					<h4>${item.name}</h4>
					<h5>${item.formatPrice}円</h5>
					<p>${item.detail}</p>
				</div>
			</div>
    <br>
        <br>
         <div align="left">
            <a href="ItemSearch?search_word=${searchWord}&page_num=${pageNum}" class="btn btn-link"><u>検索結果へ</u></a>
         </div>
         <br>
         <form action="ItemAdd" method="POST">
				<input type="hidden" name="item_id" value="${item.id}">
				    <button class="btn btn-info" type="submit" name="action">　カートに追加　</button>
            </form>
            <br>
    </div>
</body>
</html>