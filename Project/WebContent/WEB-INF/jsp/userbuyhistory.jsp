<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>購入履歴</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">
</head>
<body style="background-color: snow">
 <nav class="navbar navbar-light" style="background-color: black;">
        <ul class="nav">
		<li class="dropdown" style="list-style: none">
            <a href="Index" class="navbar-link index-link" style="color: white; font-size: 40px"><p class="sample09; font-italic">COCO CLOSET</p></a></li>　
           </ul>
		<ul class="nav justify-content-end">
            <li class="navbar-text" style="color: darkgray">${userInfo.name}さん
			</li>　　
			<li class="dropdown"><a href="Logout"
				class="navbar-link logout-link" style="color: #ff3300;">ログアウト</a></li>
		</ul>
	</nav>
     <div align="center">
    <br>
         <div align="left">
		<a class="btn btn-link" href = "UserData">
			<u>戻る</u>
		</a>
	</div>
         <br>
			<h2 class=" col s12 light">購入詳細</h2>
         <br>
         <!--  購入 -->
		<div class="container">
		<div class="row">
			<div class="section"></div>
			 <div class="col s8 offset-s2">
				<div class="card grey lighten-5" style="background-color: ghostwhite">
					<div class="card-content">
                        <br>
						<table>
							<thead>
								<tr>
                                    <table class="table">
                                    <thead>
                                        <tr>
                                          <th scope="col">購入日時</th>
                                          <th scope="col">配送方法</th>
                                          <th scope="col">合計金額</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <tr>
                                          <td>${resultBDB.formatDate}</td>
                                          <td>${resultBDB.deliveryMethodName}</td>
                                          <td>${resultBDB.formatTotalPrice}円</td>
                                        </tr>
                                      </tbody>
                                    </table>
                                </tr>
                            </thead>
                        </table>
					</div>
				</div>
			</div>
		</div>
         <br>
            <br>
		<!-- 詳細 -->
		<div class="container">
		<div class="row">
			<div class="section"></div>
			 <div class="col s8 offset-s2">
				<div class="card grey lighten-5" style="background-color: ghostwhite">
					<div class="card-content">
                        <br>
						<table>
							<thead>
								<tr>
                                    <table class="table">
                                    <thead>
                                        <tr>
                                          <th scope="col">商品名</th>
                                          <th scope="col">単価</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                    <c:forEach var="idbb" items="${resulteBDB}">
								      <tr>
									    <td class="center">${idbb.name}</td>
									    <td class="center">${idbb.price}円</td>
								      </tr>
								    </c:forEach>
                                          <tr>
                                          <td>${resultBDB.deliveryMethodName}</td>
                                            <td>${resultBDB.deliveryMethodPrice}円</td>
                                          </tr>
                                      </tbody>
                                    </table>
                                </tr>
                            </thead>
                        </table>
					</div>
				</div>
			</div>
		</div>
            </div>
            <br>
         </div>
    </div>
</body>
</html>