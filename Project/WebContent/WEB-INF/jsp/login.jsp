<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/test.css">
</head>
<body style="background-color: snow">
     <nav class="navbar navbar-light" style="background-color: black;">
        <div class="nav">
		<li class="dropdown" style="list-style: none">
            <a href="Index" class="navbar-link index-link" style="color: white; font-size: 40px"><p class="sample09; font-italic">COCO CLOSET</p></a></li>　
            <li class="nav-item" style="list-style: none">
            <a class="nav-link active" href="Regist" style="font-size: 17px">新規登録<span class="sr-only">(current)</span></a>
      </li>
            </div>
		<div class="nav justify-content-end">
			<li class="dropdown"><a href="Logout"
				class="navbar-link logout-link" style="color: #ff3300;">ログアウト</a></li>
		</div>
	</nav>
  <br>
    <br>
     <br>
     <c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert" style="color: #ff3300;">${errMsg}</div>
		</c:if>
    <form action="Login" method="POST">
        <section class="card">
         <div class="card-content" style="background-color: ghostwhite">
            <br>
    <ul>
        <li class="name">
            <label for="name">LoginID</label><br>
                <input id="name" type="text" name="loginId" placeholder="ログインID" size="30">
            </li>
            <br>
        <li class="pass">
            <label for="pass">Password</label><br>
                <input id="pass" type="password" name="password" placeholder="パスワード" size="30" 　value="Password">
            </li>
            <br>
        <br>
        <li>
            <input type="submit" id="button" name="button" class="btn btn-info" value="ログイン">
        </li>
    </ul>
            </div>
    </section>
        </form>
</body>
</html>