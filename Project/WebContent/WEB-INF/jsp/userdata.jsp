<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ情報参照</title>
<link rel="stylesheet"href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">
</head>
<body style="background-color: snow">
 <nav class="navbar navbar-light" style="background-color: black;">
        <ul class="nav">
		<li class="dropdown" style="list-style: none">
            <a href="Index" class="navbar-link index-link" style="color: white; font-size: 40px"><p class="sample09; font-italic">COCO CLOSET</p></a></li>　
           </ul>
		<ul class="nav justify-content-end">
            <li class="navbar-text" style="color: darkgray">${userInfo.name}さん
			</li>　　
			<li class="dropdown"><a href="Logout"
				class="navbar-link logout-link" style="color: #ff3300;">ログアウト</a></li>
		</ul>
	</nav>
    <br>
	   <br>
	       <br>
    <div align="center">
		<h2>会員情報</h2>
		<br>
        <section>
		<div class="container">
		<div class="row center">
			<h5 class=" col s12 light">ユーザー情報</h5>
		</div>
		<div class="row">
			<div class="col s12">
				<div class="card grey lighten-5" style="background-color: ghostwhite">
					<div class="card-content">
						<form action="UserDataUpdate" method="GET">
						<c:if test="${validationMessage != null}">
								<p class="red-text center-align">${validationMessage}</p>
							</c:if>
							<br>
                                <br>
							<div class="row">
								<div class="input-field col s6">
                                    <label>ログインID :　${udbb.loginId}</label>
								</div>
								<div class="input-field col s6">
									<label>名前 :　${udbb.name}</label>
								</div>
                                <div class="input-field col s6">
									<label>生年月日 : 　${udbb.birthDate}</label>
								</div>
							</div>
							<div class="row">
								<div class="input-field col s12">
									<label>住所 :　${udbb.address}</label>
								</div>
							</div>
                            <div class="row">
								<div class="input-field col s6">
                                    <label>登録日時 : 　${udbb.createDate}</label>
								</div>
								<div class="input-field col s6">
									<label>更新日時 : 　${udbb.updateDate}</label>
								</div>
                            </div>
                            <br>
							<div class="row">
								<div class="col s12">
									<p>
				                       <button type="submit" class="btn btn-info">　更新　</button>
			                         </p>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
    </div>
        </section>
         <br>
        <!-- 詳細 -->
        <section>
		 <hr>
            <br>
            <div class="col s12">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th style="width: 10%"></th>
                    <th scope="col">購入日時</th>
                    <th scope="col">配送方法</th>
                    <th scope="col">購入金額</th>
                </tr>
            </thead>
        <tbody>
            <c:forEach var="idb" items="${resultBDB}">
                <tr>
                	<td><a href="UserBuyHistory?buyId=${idb.id}"
				    class="btn-floating btn waves-effect waves-light ">📖</a></td>
                    <td>${idb.formatDate}</td>
                    <td>${idb.deliveryMethodName}</td>
                    <td>${idb.formatTotalPrice}円</td>
                </tr>
            </c:forEach>
         </tbody>
        </table>
        </div>
            <br>
         </section>
    </div>
</body>
</html>