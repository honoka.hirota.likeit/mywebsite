<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>商品新規登録</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">
</head>
<body style="background-color: snow">
 <nav class="navbar navbar-light" style="background-color: black;">
        <ul class="nav">
		<li class="dropdown" style="list-style: none">
            <a href="Index" class="navbar-link index-link" style="color: white; font-size: 40px"><p class="sample09; font-italic">COCO CLOSET</p></a></li>　
           </ul>
		<ul class="nav justify-content-end">
            <li class="navbar-text" style="color: darkgray">${userInfo.name}さん
			</li>　　
			<li class="dropdown"><a href="Logout"
				class="navbar-link logout-link" style="color: #ff3300;">ログアウト</a></li>
		</ul>
	</nav>
    <br>
        <br>
    <br>
     <div align="center">
         <h2>商品　新規登録</h2>
		<br>
		<c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert" style="color: #ff3300;">${errMsg}</div>
		</c:if>
         <br>
		<form class="form-signin" action="MItemRegist" method="post" enctype="multipart/form-data">
			<div>
				<label>商品名 　　<input type="text" name="name"></label>
			</div>
			<div>
				<label>商品詳細 　<input type="text" name="detail"></label>
			</div>
			<div>
				<label>価格 　　<input type="number" name="price">　円</label>
			</div>
            <br>
            <div class="element_wrap">
            <label>商品画像</label>　
                <input type="file" name="file_name">
            </div>
			<br>
                <br>
            <p>
				<button type="submit" class="btn btn-info">　登録　</button>
			</p>
		</form>
	</div>
	<div align="left">
		<a class="btn btn-link" href = "MItemList">
			<u>戻る</u>
		</a>
	</div>
</body>
</html>