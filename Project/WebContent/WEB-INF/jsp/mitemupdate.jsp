<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>アイテム情報更新</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">
</head>
<body style="background-color: snow">
 <nav class="navbar navbar-light" style="background-color: black;">
        <ul class="nav">
		<li class="dropdown" style="list-style: none">
            <a href="Index" class="navbar-link index-link" style="color: white; font-size: 40px"><p class="sample09; font-italic">COCO CLOSET</p></a></li>　
           </ul>
		<ul class="nav justify-content-end">
            <li class="navbar-text" style="color: darkgray">${userInfo.name}さん
			</li>　　
			<li class="dropdown"><a href="Logout"
				class="navbar-link logout-link" style="color: #ff3300;">ログアウト</a></li>
		</ul>
	</nav>
    <br>
	   <br>
	       <br>
	<div align="center">
		<h2>アイテム情報更新</h2>
		<br>
		<form class="form-signin" action="MItemUpdate" method="post" enctype="multipart/form-data">
		<c:if test="${validationMessage != null}">
								<p class="red-text center-align">${validationMessage}</p>
							</c:if>
			<div>
				<label>商品ID : 　${item.id}</label>
				<input type="hidden" name="id" value="${item.id}">
			</div>
			<div>
				<label>商品名</label>　<input type="text" name="name" value="${item.name}">
			</div>
			<div>
				<label>商品詳細</label>　<input type="text" name="detail" value="${item.detail}">
			</div>
			<div>
				<label>価格</label>　
				<input type="text" name="price" value="${item.price}">　円
			</div>
			 <div class="element_wrap">
            <label>商品画像</label>　
                <input type="file" name="file_name">
                <div><img src="img/${item.fileName}" width="128" height="128" ></div>
            </div>
			<br>
			<p>
               <button type="submit" class="btn btn-info">　更新　</button>
			 </p>
		</form>
		<br>
	</div>
	<div align="left">
		<a class="btn btn-link" href="MItemList"> <u>戻る</u>
		</a>
	</div>
</body>
</html>