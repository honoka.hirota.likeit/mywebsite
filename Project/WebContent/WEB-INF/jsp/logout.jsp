<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
<title>ログアウト</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body style="background-color: snow">
        <nav class="navbar navbar-light" style="background-color: black;">
        <ul class="nav">
		<li class="dropdown" style="list-style: none">
            <a href="Index" class="navbar-link index-link" style="color: white; font-size: 40px"><p class="sample09; font-italic">COCO CLOSET</p></a></li>　
           </ul>
	</nav>
    <br>
    <br>
    <br>
    <div align="center">
   <div class="container">
		<div class="row">

			<div class="section"></div>
			<div class="col">
				<div class="card grey lighten-5" style="background-color: ghostwhite">
					<div class="card-content">
                        <br>
							<h4>ログアウトしました</h4>
						</div>
                    <br>
						<div class="row">
							<div class="col s12">
								<p class="center-align">
									<a href = "Login"><button type="submit" class="btn btn-info center">　　　ログインページへ　　　</button></a>
								</p>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
    </div>
</body>
</html>