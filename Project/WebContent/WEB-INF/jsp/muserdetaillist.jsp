<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ情報一覧</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">
</head>
<body style="background-color: snow">
 <nav class="navbar navbar-light" style="background-color: black;">
        <ul class="nav">
		<li class="dropdown" style="list-style: none">
            <a href="Index" class="navbar-link index-link" style="color: white; font-size: 40px"><p class="sample09; font-italic">COCO CLOSET</p></a></li>　
           </ul>
		<ul class="nav justify-content-end">
            <li class="navbar-text" style="color: darkgray">${userInfo.name}さん
			</li>　　
			<li class="dropdown"><a href="Logout"
				class="navbar-link logout-link" style="color: #ff3300;">ログアウト</a></li>
		</ul>
	</nav>
    <br>
    <div align="left">
		<a class="btn btn-link" href = "Index">
			<u>戻る</u>
		</a>
	</div>
            <br>
        <div align="center">
		<h2>会員一覧</h2>
        <br>
            <br>
         <form class="form-signin" action="MUserDetailList" method="post">
         <c:if test="${validationMessage != null}">
			<p class="red-text center-align">${validationMessage}</p>
		</c:if>
           <div>
           <label>ログインID　　　　
                	<input type="text" name="loginId"></label>
            	</div>
            <div>
            <label>ユーザ名　　　　
                	<input type="text" name="name"></label>
            	</div>
            <div>
            <label>生年月日　　
                    <input type="date" value="年/月/日" name="birthDate1">
                </label>
            </div>
             <br>
                  <button type="submit" class="btn btn-info">　検索　</button>
              </form>
            <br>
            </div>
            <hr>
          <div class="table-responsive">
             <table class="table table-striped">
               <thead>
                <tr>
                  <th>ログインID</th>
                  <th>ユーザ名</th>
                  <th>生年月日</th>
                  <th></th>
                </tr>
 			   </thead>
               <tbody>
                 <c:forEach var="user" items="${userList}" >
                   <tr>
                     <td>${user.loginId}</td>
                     <td>${user.name}</td>
                     <td>${user.birthDate}</td>
                     <td>
                     </td>
                   </tr>
                 </c:forEach>
               </tbody>
             </table>
           </div>
</body>
</html>