<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>購入確認</title>
<link rel="stylesheet"
href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">
</head>
<body style="background-color: snow">
 <nav class="navbar navbar-light" style="background-color: black;">
        <ul class="nav">
		<li class="dropdown" style="list-style: none">
            <a href="Index" class="navbar-link index-link" style="color: white; font-size: 40px"><p class="sample09; font-italic">COCO CLOSET</p></a></li>　
           </ul>
		<ul class="nav justify-content-end">
            <li class="navbar-text" style="color: darkgray">${userInfo.name}さん
			</li>　　
			<li class="dropdown"><a href="Logout"
				class="navbar-link logout-link" style="color: #ff3300;">ログアウト</a></li>
		</ul>
	</nav>
     <div align="center">
         <br>
         <br>
	<br>
	<div class="container">
		<div class="row center">
			<h2 class="col s12 light">購入確認</h2>
		</div>
        <br>
        <br>
        <table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">商品名</th>
      <th scope="col">単価</th>
      <th scope="col">小計</th>
    </tr>
  </thead>
  <tbody>
      <c:forEach var="cartInItem" items="${cart}">
    <tr>
      <td>${cartInItem.name}</td>
      <td>${cartInItem.formatPrice}円</td>
      <td>${cartInItem.formatPrice}円</td>
    </tr>
    </c:forEach>
    <tr>
      <td>${bdb.deliveryMethodName}</td>
      <td></td>
      <td>${bdb.deliveryMethodPrice}円</td>
    </tr>
    <tr>
      <td></td>
      <td>合計</td>
      <td>${bdb.formatTotalPrice}円</td>
    </tr>
  </tbody>
</table>
    <br>
        <div class="row">
            <div class="col s12">
				<form action="BuyResult" method="post">
				    <button type="submit" class="btn btn-info">　購入　</button>
				</form>
                <br>
            </div>
        </div>
    </div>
</div>
    <div align="left">
		<a class="btn btn-link" href = "Buy">
			<u>戻る</u>
		</a>
	</div>
</body>
</html>