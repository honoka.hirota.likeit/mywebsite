package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.bind.DatatypeConverter;

import base.DBManager;
import beans.UserDataBeans;
import development.DevelopmentHelper;

public class UserDAO {

		//ログインIDとパスワードに紐づくユーザ情報を返す
		public UserDataBeans findByLoginInfo(String loginId, String password) {

			Connection conn = null;
			try {
				// データベースへ接続
				conn = DBManager.getConnection();

				//ここに処理を書いていく
				//確認済みのSQL
				String sql = "SELECT * FROM t_user WHERE login_id = ? and login_password = ?";

				//SELECTを実行し、結果表を取得
				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, loginId);
				pStmt.setString(2, password);
				ResultSet rs = pStmt.executeQuery();

				//主キーに紐づくレコードは１件のみなので、rs.next()は一回だけ行う
				//ログイン失敗時の処理
				if (!rs.next()) {
					return null;
				}

				//ログイン成功時の処理
				String loginIdData = rs.getString("login_id");
				String nameData = rs.getString("name");
				return new UserDataBeans(loginIdData, nameData);

			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
		}


	//データの挿入処理を行う。現在時刻は挿入直前に生成
	public static void insertUser(UserDataBeans udb) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("INSERT INTO t_user(name,login_id,address,login_password,create_date) VALUES(?,?,?,?,?)");
			st.setString(1, udb.getName());
			st.setString(2, udb.getLoginId());
			st.setString(3, udb.getAddress());
			st.setString(4, DevelopmentHelper.getSha256(udb.getPassword()));
			st.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
			st.executeUpdate();
			System.out.println("inserting user has been completed");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//IDを取得
	public static int getId(String loginId, String password) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM t_user WHERE login_id = ? and login_password = ?");
			st.setString(1, loginId);
			st.setString(2, password);

			ResultSet rs = st.executeQuery();

			int Id = 0;
			while (rs.next()) {
				if (DevelopmentHelper.getSha256(password).equals(rs.getString("login_password"))) {
					Id = rs.getInt("id");
					System.out.println("login succeeded");
					break;
				}
			}

			System.out.println("searching Id by loginId has been completed");
			return Id;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//ログインIDからユーザー情報を取得する
	public static UserDataBeans getUserDataBeansByLoginId(UserDataBeans udb) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT id, name, login_id, birth_date, address, create_date, update_date FROM t_user WHERE login_id = ?");
			st.setString(1, udb.getLoginId());
			ResultSet rs = st.executeQuery();

			while (rs.next()) {
				udb.setId(rs.getInt("id"));
				udb.setName(rs.getString("name"));
				udb.setLoginId(rs.getString("login_id"));
				udb.setBirthDate(rs.getDate("birth_date"));
				udb.setAddress(rs.getString("address"));
				udb.setCreateDate(rs.getDate("create_date"));
				udb.setUpdateDate(rs.getDate("update_date"));
				System.out.println();
			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

		System.out.println("searching UserDataBeans by loginId has been completed");
		return udb;
	}

	//ユーザー情報の更新処理を行う
	public static void updateUser(UserDataBeans udb) throws SQLException {
		// 更新された情報をセットされたJavaBeansのリスト
		UserDataBeans updatedUdb = new UserDataBeans();
		Connection con = null;
		PreparedStatement st = null;

		try {

			con = DBManager.getConnection();
			st = con.prepareStatement("UPDATE t_user SET name=?, login_password=?, birth_date=?, address=? WHERE id=?;");
			st.setString(1, udb.getName());
			st.setString(2, udb.getPassword());
			st.setDate(3, (java.sql.Date) udb.getBirthDate());
			st.setString(4, udb.getAddress());
			st.setString(5, udb.getLoginId());
			st.executeUpdate();
			System.out.println("update has been completed");

			st = con.prepareStatement("SELECT * FROM t_user WHERE id=" + udb.getId());
			ResultSet rs = st.executeQuery();

			while (rs.next()) {
				updatedUdb.setName(rs.getString("name"));
				updatedUdb.setLoginId(rs.getString("loginid"));
				updatedUdb.setAddress(rs.getString("address"));
				updatedUdb.setAddress(rs.getString("password"));
				updatedUdb.setAddress(rs.getString("birthdate"));
				updatedUdb.setAddress(rs.getString("createdate"));
				updatedUdb.setAddress(rs.getString("updatedate"));
			}

			st.close();
			System.out.println("searching updated-UserDataBeans has been completed");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//loginIdの重複チェック
	public static boolean isOverlapLoginId(String loginId, int userId) throws SQLException {
		// 重複しているかどうか表す変数
		boolean isOverlap = false;
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			// 入力されたlogin_idが存在するか調べる
			st = con.prepareStatement("SELECT login_id FROM t_user WHERE login_id = ? AND id != ?");
			st.setString(1, loginId);
			st.setInt(2, userId);
			ResultSet rs = st.executeQuery();

			System.out.println("searching loginId by inputLoginId has been completed");

			if (rs.next()) {
				isOverlap = true;
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

		System.out.println("overlap check has been completed");
		return isOverlap;
	}

	//ユーザ情報一覧取得
	public static ArrayList<UserDataBeans> getUserDataBeans() throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM t_user");

			ResultSet rs = st.executeQuery();
			ArrayList<UserDataBeans> userDataList = new ArrayList<UserDataBeans>();

			while (rs.next()) {
				UserDataBeans idb = new UserDataBeans();
				idb.setName(rs.getString("name"));
				idb.setAddress(rs.getString("address"));
				idb.setLoginId(rs.getString("login_id"));
				idb.setPassword(rs.getString("login_password"));
				idb.setId(rs.getInt("id"));
				idb.setBirthDate(rs.getDate("birth_date"));
				idb.setCreateDate(rs.getTimestamp("create_date"));
				idb.setUpdateDate(rs.getDate("update_date"));

				userDataList.add(idb);
			}

			System.out.println("searching ItemDataBeansList by BuyID has been completed");
			return userDataList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//ユーザ情報一覧検索
		public static ArrayList<UserDataBeans> getUserDataBeansSearch(String loginId, String name, String birthDate1) throws SQLException {

			Connection con = null;
			ArrayList<UserDataBeans> userDataList = new ArrayList<UserDataBeans>();

			try {
				//データベースへ接続
				con = DBManager.getConnection();

				// SELECT文を準備
				String sql = "SELECT * FROM t_user WHERE login_id NOT IN ('admin')";

				if(!loginId.equals("")) {
					sql+=" AND login_id = '"+loginId+"'";
				}

				if(!name.equals("")) {
					sql+=" AND name = '"+name+"'";
				}

				if(!birthDate1.equals("")) {
					sql+=" AND birth_date = '"+birthDate1+"'";
				}

				//SELECTを実行し、結果表を取得
				Statement stmt = con.createStatement();
				System.out.println(sql);

				ResultSet rs = stmt.executeQuery(sql);

				//ログイン成功時の処理
				while (rs.next()) {
					int id = rs.getInt("id");
					loginId = rs.getString("login_id");
					String password = rs.getString("login_password");
					name = rs.getString("name");
					Date birthDate = rs.getDate("birth_date");
					String address = rs.getString("address");
					Date createDate = rs.getDate("create_date");
					Date updateDate = rs.getDate("update_date");
					UserDataBeans idb = new UserDataBeans(id, loginId,  password, name, birthDate, address,createDate, updateDate);

					userDataList.add(idb);
				}

				System.out.println("searching ItemDataBeansList by BuyID has been completed");
				return userDataList;
			} catch (SQLException e) {
				System.out.println(e.getMessage());
				throw new SQLException(e);
			} finally {
				if (con != null) {
					con.close();
				}
			}
		}
		public String userMd(String password) {

			String re = "";

			try {
				//ハッシュを生成したい元の文字列
				String source = password;

				//ハッシュ生成前にバイト配列に置き換える際のCharset
				Charset charset = StandardCharsets.UTF_8;

				//ハッシュアルゴリズム
				String algorithm = "MD5";

				//ハッシュ生成処理
				byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
				re = DatatypeConverter.printHexBinary(bytes);

				//標準出力
				System.out.println(re);

			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
			return re;
		}

		public UserDataBeans Registered(String loginId) throws SQLException{

			Connection conn = null;
			try {
				// データベースへ接続
				conn = DBManager.getConnection();

				// SELECT文を準備
				String sql = "SELECT * FROM t_user WHERE login_id = ?";

				// SELECTを実行
				PreparedStatement stmt = conn.prepareStatement(sql);
				stmt.setString(1, loginId);
				ResultSet rs = stmt.executeQuery();

				if (!rs.next()) {
					return null;
				}

				//ログイン成功時の処理
				int idData = rs.getInt("id");
				String loginIdData = rs.getString("login_id");
				String passwordData = rs.getString("password");
				String nameData = rs.getString("name");
				Date birthDateData = rs.getDate("birth_date");
				String addressData = rs.getString("address");
				Date createDateData = rs.getDate("create_date");
				Date updateDateData = rs.getDate("update_date");
				return new UserDataBeans(idData, loginIdData, passwordData, nameData, birthDateData, addressData, createDateData, updateDateData);

			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
		}

		public void userRegistration(String loginId, String password, String name, String birthDate, String address) throws SQLException{

			Connection conn = null;
			try {
				// データベースへ接続
				conn = DBManager.getConnection();

				// INSERT文を準備
				String sql = "INSERT INTO t_user (login_id,login_password,name,birth_date,address,create_date,update_date) VALUES (?, ?, ?, ?, ?, now(), now())";

				// INSERTを実行
				PreparedStatement stmt = conn.prepareStatement(sql);
				stmt.setString(1, loginId);
				stmt.setString(2, password);
				stmt.setString(3, name);
				stmt.setString(4, birthDate);
				stmt.setString(5, address);
				stmt.executeUpdate();

			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}

		public void userUpDatetwo(String name, String password, String birthDate, String address, String loginId) {

			Connection conn = null;
			try {
				// データベースへ接続
				conn = DBManager.getConnection();

				//確認済みのSQL
				String sql = "UPDATE t_user SET name = ?, login_password = ?, birth_date = ?, address = ?, update_date = now() WHERE login_id = ?";

				//SELECTを実行し、結果表を取得
				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, name);
				pStmt.setString(2, password);
				pStmt.setString(3, birthDate);
				pStmt.setString(4, address);
				pStmt.setString(5, loginId);
				pStmt.executeUpdate();

			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}

			}
		}

}