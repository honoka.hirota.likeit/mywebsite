package development;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import beans.ItemDataBeans;
import dao.ItemDAO;

/**
 * 商品登録画面
 * Servlet implementation class MItemRegist
 */
@WebServlet("/MItemRegist")
@MultipartConfig(location="C:\\Users\\81903\\Documents\\MyWebSite\\Project\\WebContent\\img", maxFileSize=1048576)
public class MItemRegist extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();

		try {

		//入力内容に誤りがある、確認画面で戻るボタン押し下げでアクセスしてきたときはセッションから入力情報を取得
		ItemDataBeans udb = session.getAttribute("udb") != null?(ItemDataBeans) DevelopmentHelper.cutSessionAttribute(session, "udb"):new ItemDataBeans();
		String validationMessage = (String) DevelopmentHelper.cutSessionAttribute(session, "validationMessage");

		request.setAttribute("udb", udb);
		request.setAttribute("validationMessage",validationMessage);

		request.getRequestDispatcher(DevelopmentHelper.ITEM_REGIST_PAGE).forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();

		try {

			// 画像情報そのものを取得
		    Part filePart = request.getPart("file_name"); // Retrieves <input type="file" name="file_name">

			ItemDAO itemDao = new ItemDAO();
			// 画像名のみを抽出している
			String file = itemDao.getFileName(filePart);

			String name = request.getParameter("name");
			String detail = request.getParameter("detail");
			String price = request.getParameter("price");

			//未入力のものがあるとき
			if (name.equals("") || detail.equals("") || price.equals("") || file.equals("")) {
				request.setAttribute("errMsg", "入力された内容は正しくありません");
				request.setAttribute("name", name);
				request.setAttribute("detail", detail);
				request.setAttribute("price", price);
				request.setAttribute("file", file);
				//新規登録画面jspにフォワード
				request.getRequestDispatcher(DevelopmentHelper.ITEM_REGIST_PAGE).forward(request, response);
				return;
			}

			//リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
			ItemDAO itemDaoo = new ItemDAO();
			itemDaoo.insertItem( name, detail, price, file);

			// @MultipartConfigで指定した場所に画像情報を保存
			filePart.write(file);
			response.sendRedirect("MItemList");


		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}
}

