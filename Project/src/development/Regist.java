package development;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * ユーザ登録画面遷移
 * Servlet implementation class Regist
 */
@WebServlet("/Regist")
public class Regist extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();

		//入力内容に誤りがある、確認画面で戻るボタン押し下げでアクセスしてきたときはセッションから入力情報を取得
		UserDataBeans udb = session.getAttribute("udb") != null?(UserDataBeans) DevelopmentHelper.cutSessionAttribute(session, "udb"):new UserDataBeans();
		String validationMessage = (String) DevelopmentHelper.cutSessionAttribute(session, "validationMessage");

		request.setAttribute("udb", udb);
		request.setAttribute("validationMessage",validationMessage);

		request.getRequestDispatcher(DevelopmentHelper.REGIST_PAGE).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();

	try{

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String confirmation = request.getParameter("confirmation");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");
		String address = request.getParameter("address");

		UserDAO userDao = new UserDAO();
		UserDataBeans user = userDao.Registered(loginId);

		request.setAttribute("user", user);

		//既に登録されているログインIDが入力されたとき
		if (!(user == null)) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			request.setAttribute("loginId", loginId);
			request.setAttribute("name", name);
			request.setAttribute("birthDate", birthDate);
			request.setAttribute("address", address);

			//新規登録画面jspにフォワード
			request.getRequestDispatcher(DevelopmentHelper.REGIST_PAGE).forward(request, response);
			return;
		}

		//パスワードと確認の入力内容が異なるとき
		if (!password.equals(confirmation)) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			request.setAttribute("loginId", loginId);
			request.setAttribute("name", name);
			request.setAttribute("birthDate", birthDate);
			//新規登録画面jspにフォワード
			request.getRequestDispatcher(DevelopmentHelper.REGIST_PAGE).forward(request, response);
			return;
		}

		//未入力のものがあるとき
		if (loginId.equals("") || password.equals("") || confirmation.equals("") || name.equals("")
				|| birthDate.equals("") || address.equals("")) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			request.setAttribute("loginId", loginId);
			request.setAttribute("name", name);
			request.setAttribute("birthDate", birthDate);
			request.setAttribute("address", address);
			//新規登録画面jspにフォワード
			request.getRequestDispatcher(DevelopmentHelper.REGIST_PAGE).forward(request, response);
			return;
		}

		//暗号化メソッド呼び出し
		UserDAO userDaomd = new UserDAO();
		String re = userDaomd.userMd(password);

		//リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		UserDAO userDaoo = new UserDAO();
		userDaoo.userRegistration(loginId, re, name, birthDate, address);

		//ユーザ一覧のサーブレットにリダイレクト
		response.sendRedirect("Login");

	} catch (Exception e) {
		e.printStackTrace();
		session.setAttribute("errorMessage", e.toString());
		response.sendRedirect("Error");
	}
}
}
