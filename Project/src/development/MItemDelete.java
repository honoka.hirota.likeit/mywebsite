package development;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.ItemDAO;


/**
 * アイテム削除
 * Servlet implementation class MItemDelete
 */
@WebServlet("/MItemDelete")
public class MItemDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();

		try {
			//選択された商品のIDを型変換し利用
			int id = Integer.parseInt(request.getParameter("id"));

			//対象のアイテム情報を取得
			ItemDataBeans item = ItemDAO.getItemByItemID(id);

			request.setAttribute("item", item);

			//ユーザ一覧のjspにフォワード
			request.getRequestDispatcher(DevelopmentHelper.ITEM_DELETE_PAGE).forward(request, response);

			} catch (Exception e) {
				e.printStackTrace();
				session.setAttribute("errorMessage", e.toString());
				response.sendRedirect("Error");
			}

		}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();

		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		try {

			//OKボタンで選択されたアイテムに紐づくデータをアイテムデータから削除する
			String Id = request.getParameter("id");

			ItemDAO itemDao = new ItemDAO();
			itemDao.itemDelete(Id);

			//削除が成功したらアイテム一覧に
			response.sendRedirect("MItemList");

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}