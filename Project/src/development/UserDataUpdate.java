package development;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * ユーザ情報更新画面遷移
 * Servlet implementation class UserDataUpdate
 */
@WebServlet("/UserDataUpdate")
public class UserDataUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();

		try {
			// ログイン時に取得したログインIDをセッションから取得
			UserDataBeans loginId = (UserDataBeans) session.getAttribute("userInfo");

			//idを引数にして、idに紐づくユーザ情報を出力する
			UserDataBeans udb =UserDAO.getUserDataBeansByLoginId(loginId);

			request.setAttribute("udb", udb);

			request.getRequestDispatcher(DevelopmentHelper.USER_DATA_UPDATE_PAGE).forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/*文字化け対策*/
		request.setCharacterEncoding("UTF-8");

		//セッション開始
		HttpSession session = request.getSession();
		try {

			String loginId = request.getParameter("loginId");
			String password = request.getParameter("password");
			String confirmation = request.getParameter("confirmation");
			String name = request.getParameter("name");
			String birthDate = request.getParameter("birthDate");
			String address = request.getParameter("address");

			//パスワードと確認の入力内容が異なるとき
			if (!password.equals(confirmation)) {
				request.setAttribute("errMsg", "入力された内容は正しくありません");
				// ログイン時に取得したログインIDをセッションから取得
				UserDataBeans Id = (UserDataBeans) session.getAttribute("userInfo");

				//idを引数にして、idに紐づくユーザ情報を出力する
				UserDataBeans udb =UserDAO.getUserDataBeansByLoginId(Id);

				request.setAttribute("udb", udb);
				//新規登録画面jspにフォワード
				request.getRequestDispatcher(DevelopmentHelper.USER_DATA_UPDATE_PAGE).forward(request, response);
				return;
			}

			//未入力のものがあるとき
			if (password.equals("") || confirmation.equals("") || name.equals("")
					|| birthDate.equals("") || address.equals("")) {
				request.setAttribute("errMsg", "入力された内容は正しくありません");
				// ログイン時に取得したログインIDをセッションから取得
				UserDataBeans Id = (UserDataBeans) session.getAttribute("userInfo");

				//idを引数にして、idに紐づくユーザ情報を出力する
				UserDataBeans udb =UserDAO.getUserDataBeansByLoginId(Id);

				request.setAttribute("udb", udb);
				//新規登録画面jspにフォワード
				request.getRequestDispatcher(DevelopmentHelper.USER_DATA_UPDATE_PAGE).forward(request, response);
				return;
			}

			 //暗号化メソッド呼び出し
			UserDAO userDaomd = new UserDAO();
			String re = userDaomd.userMd(password);

			//リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
			UserDAO udb = new UserDAO();
			udb.userUpDatetwo(name, re, birthDate, address, loginId);

			request.setAttribute("returnUDB", udb);

			response.sendRedirect("UserData");

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}
