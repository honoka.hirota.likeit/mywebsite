package development;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.ItemDAO;

/**
 * 商品一覧
 * Servlet implementation class MItemList
 */
@WebServlet("/MItemList")
public class MItemList extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("static-access")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

			HttpSession session = request.getSession();

			try {

				//アイテム一覧情報を取得
				ItemDAO itemDao = new ItemDAO();
				ArrayList<ItemDataBeans> itemList = itemDao.getItemDataBeansList();

				//リクエストスコープにアイテム一覧情報をセット
				request.setAttribute("itemList", itemList);

				request.getRequestDispatcher(DevelopmentHelper.ITEM_LIST_PAGE).forward(request, response);

			} catch (Exception e) {
				e.printStackTrace();
				session.setAttribute("errorMessage", e.toString());
				response.sendRedirect("Error");
			}
	}

	@SuppressWarnings("static-access")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

			// リクエストパラメータの文字コードを指定
			request.setCharacterEncoding("UTF-8");

			HttpSession session = request.getSession();

			try {

				String searchWord = request.getParameter("search_word");

				// 新たに検索されたキーワードをセッションに格納する
				session.setAttribute("searchWord", searchWord);

				//リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
				ItemDAO itemDao = new ItemDAO();
				ArrayList<ItemDataBeans> itemList = itemDao.getItemDataBeansListSearch(searchWord);

				request.setAttribute("itemList", itemList);

				// 入力された内容に誤りがあったとき等に表示するエラーメッセージを格納する
				String validationMessage = (String) DevelopmentHelper.cutSessionAttribute(session, "validationMessage");

				request.setAttribute("validationMessage", validationMessage);

				request.getRequestDispatcher(DevelopmentHelper.ITEM_LIST_PAGE).forward(request, response);

			} catch (Exception e) {
				e.printStackTrace();
				session.setAttribute("errorMessage", e.toString());
				response.sendRedirect("Error");
			}
	}
}