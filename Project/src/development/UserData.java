package development;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.UserDataBeans;
import dao.BuyDAO;
import dao.UserDAO;

/**
 * ユーザ情報画面遷移
 * Servlet implementation class UserData
 */
@WebServlet("/UserData")
public class UserData extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();

		try {
			// ログイン時に取得したログインIDをセッションから取得
			UserDataBeans loginId = (UserDataBeans) session.getAttribute("userInfo");

			// 更新確認画面から戻ってきた場合Sessionから取得。それ以外はloginIdでユーザーを取得
			UserDataBeans udbb = session.getAttribute("returnUDB") == null ? UserDAO.getUserDataBeansByLoginId(loginId) : (UserDataBeans) DevelopmentHelper.cutSessionAttribute(session, "returnUDB");

			//------↓下の部分------

			//購入情報を取得
			ArrayList<BuyDataBeans> resultBDB = BuyDAO.getBuyDataBeansByUserId(udbb.getId());
			request.setAttribute("resultBDB", resultBDB);

			// 入力された内容に誤りがあったとき等に表示するエラーメッセージを格納する
			String validationMessage = (String) DevelopmentHelper.cutSessionAttribute(session, "validationMessage");

			request.setAttribute("validationMessage", validationMessage);
			request.setAttribute("udbb", udbb);

			request.getRequestDispatcher(DevelopmentHelper.USER_DATA_PAGE).forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}
