package development;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * ログイン画面遷移
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//ログインセッションがある場合、ユーザ一覧画面にリダイレクトされる(もうログインしてますよ）
		HttpSession session = request.getSession();
		UserDataBeans user = (UserDataBeans) session.getAttribute("userInfo");

			if (user != null) {
				response.sendRedirect("Index");
				return;
			}

		request.getRequestDispatcher(DevelopmentHelper.LOGIN_PAGE).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");

		 //暗号化メソッド呼び出し
		UserDAO userDaomd = new UserDAO();
		String re = userDaomd.userMd(password);

		//リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		UserDAO userDao = new UserDAO();
		UserDataBeans user = userDao.findByLoginInfo(loginId, re);

		//テーブルに該当のデータが見つからなかった場合(ログイン失敗時)
		if (user == null) {
			//リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "ログインIDまたはパスワードが異なります");
			//ログインjspにフォワード
			request.getRequestDispatcher(DevelopmentHelper.LOGIN_PAGE).forward(request, response);
			return;
		}

		//見つかった場合(ログイン成功時)
		//セッションにユーザの情報をセット
		HttpSession session = request.getSession();
		session.setAttribute("userInfo", user);

		//ユーザ一覧のサーブレットにリダイレクト
		response.sendRedirect("Index");

	}
}