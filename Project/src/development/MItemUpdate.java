package development;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import beans.ItemDataBeans;
import dao.ItemDAO;


/**
 * アイテム更新
 * Servlet implementation class MItemUpdate
 */
@WebServlet("/MItemUpdate")
@MultipartConfig(location="C:\\Users\\81903\\Documents\\MyWebSite\\Project\\WebContent\\img", maxFileSize=1048576)
public class MItemUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();

		try {

			//選択された商品のIDを型変換し利用
			int id = Integer.parseInt(request.getParameter("id"));

			//対象のアイテム情報を取得
			ItemDataBeans item = ItemDAO.getItemByItemID(id);

			//リクエストパラメーターにセット
			request.setAttribute("item", item);

			//フォワード
			request.getRequestDispatcher(DevelopmentHelper.ITEM_UPDATE_PAGE).forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/*文字化け対策*/
		request.setCharacterEncoding("UTF-8");

		//セッション開始
		HttpSession session = request.getSession();
		try {
			//エラーメッセージを格納する変数
			String validationMessage = "";

			// 画像情報そのものを取得
		    Part filePart = request.getPart("file_name"); // Retrieves <input type="file" name="file_name">

			ItemDAO itemDao = new ItemDAO();
			// 画像名のみを抽出している
			String file = itemDao.getFileName(filePart);

			//選択された商品のIDを型変換し利用
			int id = Integer.parseInt(request.getParameter("id"));

			String name = request.getParameter("name");
			String detail = request.getParameter("detail");
			String price = request.getParameter("price");

			//セッションにエラーメッセージを持たせてアップデート画面へ
			if(!(validationMessage.length() == 0)){
				session.setAttribute("validationMessage", validationMessage);
				response.sendRedirect("MItemUpdate");
			}

			//リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
			ItemDAO itemDaoo = new ItemDAO();
			itemDaoo.itemUpDate(name,detail,price,file,id);

			// @MultipartConfigで指定した場所に画像情報を保存
			filePart.write(file);

			//アイテム情報画面にリダイレクト
			response.sendRedirect("MItemList");

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}
