package development;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * ユーザ一覧
 * Servlet implementation class MUserDetailList
 */
@WebServlet("/MUserDetailList")
public class MUserDetailList extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("static-access")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

			HttpSession session = request.getSession();

			try {

				//ユーザ一覧情報を取得
				UserDAO userDao = new UserDAO();
				ArrayList<UserDataBeans> userList = userDao.getUserDataBeans();

				//リクエストスコープにユーザ一覧情報をセット
				request.setAttribute("userList", userList);

				request.getRequestDispatcher(DevelopmentHelper.USER_LIST_PAGE).forward(request, response);

			} catch (Exception e) {
				e.printStackTrace();
				session.setAttribute("errorMessage", e.toString());
				response.sendRedirect("Error");
			}
	}

	@SuppressWarnings("static-access")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

			// リクエストパラメータの文字コードを指定
			request.setCharacterEncoding("UTF-8");

			HttpSession session = request.getSession();

			try {

				String loginId = request.getParameter("loginId");
				String name = request.getParameter("name");
				String birthDate1 = request.getParameter("birthDate1");

				//リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
				UserDAO userDao = new UserDAO();
				ArrayList<UserDataBeans> userList = userDao.getUserDataBeansSearch(loginId, name, birthDate1);

				request.setAttribute("userList", userList);

				// 入力された内容に誤りがあったとき等に表示するエラーメッセージを格納する
				String validationMessage = (String) DevelopmentHelper.cutSessionAttribute(session, "validationMessage");

				request.setAttribute("validationMessage", validationMessage);

				request.getRequestDispatcher(DevelopmentHelper.USER_LIST_PAGE).forward(request, response);

			} catch (Exception e) {
				e.printStackTrace();
				session.setAttribute("errorMessage", e.toString());
				response.sendRedirect("Error");
			}
	}
}
