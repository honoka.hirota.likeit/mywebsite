package development;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.ItemDataBeans;
import dao.BuyDAO;
import dao.BuyDetailDAO;

/**
 * 購入履歴画面遷移
 * Servlet implementation class UserBuyHistory
 */
@WebServlet("/UserBuyHistory")
public class UserBuyHistory extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();

		try {

			//URLからGETパラメータとしてIDを受け取る
			String buyId = request.getParameter("buyId");

			//購入IDを引数にして、購入IDに紐づく情報を出力する
			BuyDataBeans resultBDB = BuyDAO.getBuyDataBeansByBuyIdBy(buyId);
			request.setAttribute("resultBDB", resultBDB);

			//------↓下の部分------

			//購入した商品の情報を取得
			ArrayList<ItemDataBeans> resulteBDB = BuyDetailDAO.getItemDataBeansListByUserId(buyId);
			request.setAttribute("resulteBDB", resulteBDB);

			// 入力された内容に誤りがあったとき等に表示するエラーメッセージを格納する
			String validationMessage = (String) DevelopmentHelper.cutSessionAttribute(session, "validationMessage");

			request.setAttribute("validationMessage", validationMessage);

			request.getRequestDispatcher(DevelopmentHelper.USER_BUY_HISTORY_PAGE).forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}
