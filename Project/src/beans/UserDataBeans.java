package beans;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;


//ユーザ

public class UserDataBeans implements Serializable{
	private String name;
	private String address;
	private String loginId;
	private String password;
	private int id;
	private Date birthDate;
	private Date createDate;
	private Date updateDate;

	//すべてのデータをセットするコンストラクタ
		public UserDataBeans(int id, String loginId, String password, String name, Date birthDate, String address, Date createDate,
				Date updateDate) {
			super();
			this.id = id;
			this.loginId = loginId;
			this.password = password;
			this.name = name;
			this.birthDate = birthDate;
			this.address = address;
			this.createDate = createDate;
			this.updateDate = updateDate;
		}


	//ログインセッションを保存するためのコンストラクタ
	public UserDataBeans(String loginId, String name) {
		this.loginId = loginId;
		this.name = name;
	}

	public UserDataBeans() {
		// TODO 自動生成されたコンストラクター・スタブ
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthdate) {
		this.birthDate = birthdate;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getFormatDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日HH時mm分");
		return sdf.format(createDate);
	}

	// ユーザー情報更新時の必要情報をまとめてセットするための処理
	public void setUpdateUserDataBeansInfo(String name, String password, Date birthdate, String address, String loginid) {
		this.name = name;
		this.password = password;
		this.birthDate = birthdate;
		this.address = address;
		this.loginId = loginid;
	}

}