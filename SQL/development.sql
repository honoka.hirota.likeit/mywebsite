CREATE DATABASE development DEFAULT CHARACTER SET utf8;
USE development;
CREATE TABLE t_user(id int(11) PRIMARY KEY AUTO_INCREMENT,name varchar(256),login_id varchar(256)UNIQUE,login_password varchar(256),birth_date date,address varchar(256),create_date datetime,update_date datetime);
CREATE TABLE t_buy(id int(11) PRIMARY KEY AUTO_INCREMENT,user_id int(11),total_price int(11),delivery_method_id int(11),create_date datetime);
CREATE TABLE t_buy_detail(id int(11) PRIMARY KEY AUTO_INCREMENT,buy_id int(11),item_id int(11));
CREATE TABLE m_delivery_method(id int(11) PRIMARY KEY AUTO_INCREMENT,name varchar(256),price int(11));
CREATE TABLE m_item(id int(11) PRIMARY KEY AUTO_INCREMENT,name varchar(256),detaile text,price int(11),file_name varchar(256));
